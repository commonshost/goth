const net = require('net')
const tls = require('tls')

const GOPHER_TAIL = Buffer.from('\r\n')

class GopherServer extends net.Server {
  constructor (options = {}, connectionListener) {
    if (!connectionListener && typeof options === 'function') {
      connectionListener = options
      options = {}
    }
    super(options, (plainSocket) => {
      plainSocket.once('readable', () => {
        const chunk = plainSocket.read()
        if (chunk === null || plainSocket.readyState !== 'open') {
          plainSocket.destroy()
        } else if (
          chunk.length >= 2 &&
          chunk
            .slice(chunk.length - 2, chunk.length)
            .compare(GOPHER_TAIL) === 0
        ) {
          plainSocket.push(chunk)
          this.emit('gopherConnection', plainSocket, 'net')
        } else {
          const secureSocket = new tls.TLSSocket(plainSocket, {
            ...options,
            ticketKeys: this.ticketKeys,
            ALPNProtocols: ['gopher'],
            isServer: true,
            server: this
          })
          secureSocket.once('secure', () => {
            if (secureSocket.alpnProtocol === false) {
              secureSocket.end(
                '3Unknown ALPN Protocol. Expected `gopher` to be available.' +
                '\t\terror.host\t1\r\n' +
                '.\r\n'
              )
            }
            this.emit('gopherConnection', secureSocket, 'tls')
          })
          plainSocket.push(chunk)
        }
      })
    })
    if (connectionListener) {
      this.on('gopherConnection', connectionListener)
    }
    if (options.ticketKeys) {
      this.ticketKeys = options.ticketKeys
    }
  }

  getTicketKeys (keys) {
    return this.ticketKeys
  }

  setTicketKeys (keys) {
    this.ticketKeys = keys
  }
}

module.exports.GopherServer = GopherServer
