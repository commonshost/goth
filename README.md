# Gopher over TLS

Gopher over TLS (GoT) server for Node.js to accept both plaintext and TLS connections on the same port (e.g. `70/tcp`).

## Video

Presented by [Sebastiaan Deckers](https://twitter.com/sebdeckers) at [WFHConf](gopher://wfhconf.commons.host) on 2020-03-26.

[![Talk about Gopher at WFHConf 2020](./wfhconf-talk.jpg)](https://www.youtube.com/watch?v=4OphAbBnZSs)

## API

### `new GopherServer([options][, gopherConnectionListener])`

The `GopherServer` class is a subclass of [`net.Server`](https://nodejs.org/api/net.html#net_class_net_server) that accepts either plaintext or TLS connections.

- `options` Same as [`net.Server`](https://nodejs.org/api/net.html#net_new_net_server_options_connectionlistener) and [`tls.Socket`](https://nodejs.org/api/tls.html#tls_new_tls_tlssocket_socket_options).

- `gopherConnectionListener` Set as listener for `gopherConnection` event.

### Event: `gopherConnection`

- `socket` Instance of either `tls.Socket` or `net.Socket`.
- `type` String that is either `tls` or `net`.

## Usage

```js
const { GopherServer } = require('goth')

const server = new GopherServer({ key, cert, ca }, (socket, type) => {
  console.log(`Connected via ${type} to domain ${socket.servername}`)
})
```

## Testing

Connect with the OpenSSL `s_client` tool using the SNI and ALPN options. As an example, the `commons.host` domain supports GoT on port `70`.

```shell
echo -ne "/\r\n" | openssl s_client -ign_eof -servername commons.host -alpn gopher -connect commons.host:70
```

`-servername commons.host` is sent in the TLS ClientHello opening packet as Server Name Identifier (SNI). This usually, but not necessarily, matches the `-connect` hostname. SNI lets the TLS server respond with the appropriate certificate for the desired domain, allowing virtual hosting of multiple domains on the same IP address.

`-alpn gopher` tells the server which protocol the client intends to speak over the TLS connection. This provides forward compatibility for protocol revisions.

## Gopher over TLS (GoT) Protocol

The Gopher over TLS (GoT) protocol is meant to be simple to implement and acts as a blind transport for the Gopher protocol. GoT supports any TCP port, including the default Gopher TCP port `70`.

A GoT client attempts a TLS handshake with `gopher` as the ALPN identifier. If the TCP/IP socket was successful but the attempt fails without receiving a `ServerHello` message, a GoT client may attempt to connect without TLS, treating the connection as plaintext Gopher. This failure may be cached for as long as the server's DNS records are valid.

A GoT server should accept both Gopher over TLS and plaintext Gopher on the same TCP port. A GoT server detects a GoT client by checking the first packet received on a socket. If the payload of the first packet ends in `CRLF` then the GoT server should handle the payload as a plaintext Gopher request. Otherwise the GoT server should attempt a TLS handshake with `gopher` as the ALPN identifier.

A GoT client must include the SNI server name. A GoT server may use the SNI server name to serve Gopher content for its indicated domain. This allows virtual hosting of several domains by a multi-tenant GoT server.

## See Also

- [Gopher over HTTP](https://gitlab.com/commonshost/goh) - GoH protocol & implementation
- [TLS Router](https://gitlab.com/commonshost/tls-router): Accept plaintext and encrypted clients on the same port. Forward traffic to one or more plaintext Gopher backend servers. With ALPN and SNI support for virtual hosting.
