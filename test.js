const tls = require('tls')
const net = require('net')
const fs = require('fs')
const test = require('blue-tape')
const { GopherServer } = require('.')
const { randomBytes } = require('crypto')

const tlsOptions = {
  ecdhCurve: 'P-384:P-256',
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem'),
  ca: [/* fs.readFileSync('ca.pem') */]
}

test('TLS set/get ticket keys', async (t) => {
  const originalTicketKeys = randomBytes(48)
  const refreshedTicketKeys = randomBytes(48)
  const server = new GopherServer({ ticketKeys: originalTicketKeys })
  t.is(server.getTicketKeys(), originalTicketKeys)
  server.setTicketKeys(refreshedTicketKeys)
  t.is(server.getTicketKeys(), refreshedTicketKeys)
  t.is(server.ticketKeys, refreshedTicketKeys)
})

test('Constructor options argument is optional', (t) => {
  const server = new GopherServer((client) => {
    client.resume()
    client.end('Goodbye.')
    server.close(t.end)
  })
  server.listen(0, () => {
    const client = net.connect(server.address().port, () => {
      client.end('/foobar' + '\r\n')
    })
  })
})

test('Constructor callback argument is optional', (t) => {
  const server = new GopherServer()
  server.on('gopherConnection', (client) => {
    client.resume()
    client.end('Goodbye.')
    server.close(t.end)
  })
  server.listen(0, () => {
    const client = net.connect(server.address().port, () => {
      client.end('/foobar' + '\r\n')
    })
  })
})

let gotServer
test('Start GoT server', (t) => {
  const serverOptions = {
    SNICallback: (servername, callback) => {
      const secureContext = tls.createSecureContext(tlsOptions)
      callback(null, secureContext)
    }
  }

  gotServer = new GopherServer(serverOptions, (socket, type) => {
    console.log(`[SERVER] Connection: type=${type}, SNI=${socket.servername}`)
    if (type === 'net') {
      socket.once('data', (chunk) => {
        console.log(`[SERVER] RECV: ${chunk}`)
        const response = '3Yeah, nah\t\t\t\r\n.\r\n'
        socket.end(response)
      })
    } else if (type === 'tls') {
      socket.once('data', (chunk) => {
        console.log(`[SERVER] RECV: ${chunk}`)
        const response = '1G\'day, mate\t\t\t\r\n.\r\n'
        socket.end(response)
      })
    }
  })

  gotServer.listen(0, () => {
    const { address, port, family } = gotServer.address()
    const host = family === 'IPv6'
      ? `[${address}]:${port}`
      : `${address}:${port}`
    console.log(`[SERVER] Listening at ${host}`)
    t.end()
  })
})

test('Gopher-au-naturel client', (t) => {
  const client = net.connect({
    host: 'localhost',
    port: gotServer.address().port
  })
  client.on('connect', () => {
    console.log('[CLIENT] connected')
    const selector = '/foo/bar\r\n'
    client.write(selector)
  })
  client.on('data', (chunk) => {
    console.log(`[CLIENT] RECV: ${chunk}`)
    t.ok(/Yeah, nah/.test(chunk))
  })
  client.on('error', ({ message }) => {
    console.error(`[CLIENT] ERRO: ${message}`)
  })
  client.on('close', t.end)
})

test('Gopher over TLS client', (t) => {
  const client = tls.connect({
    host: 'localhost',
    port: gotServer.address().port,
    servername: 'example.com',
    ALPNProtocols: ['gopher'],
    rejectUnauthorized: false
  })
  client.on('secureConnect', () => {
    console.info('[CLIENT] connected')
    const selector = '/foo/bar\r\n'
    client.write(selector)
  })
  client.on('data', (chunk) => {
    console.log(`[CLIENT] RECV: ${chunk}`)
    t.ok(/G'day, mate/.test(chunk))
  })
  client.on('error', ({ message }) => {
    console.error(`[CLIENT] ERRO: ${message}`)
  })
  client.on('close', t.end)
})

test('Non-ALPN TLS client connecting to GoT server', (t) => {
  const client = tls.connect({
    host: 'localhost',
    port: gotServer.address().port,
    servername: 'example.com',
    ALPNProtocols: '',
    rejectUnauthorized: false
  })
  client.on('data', (chunk) => {
    console.log(`[CLIENT] RECV: ${chunk}`)
    t.ok(/Unknown ALPN Protocol/.test(chunk))
  })
  client.on('close', t.end)
})

test('TCP client port scanning the GoT server', (t) => {
  const client = net.connect({
    host: 'localhost',
    port: gotServer.address().port
  })
  client.on('connect', () => {
    console.log('[CLIENT] connected')
    client.end()
  })
  client.on('close', t.end)
})

test('Stop GoT server', (t) => {
  gotServer.close(t.end)
})

const gopherServer = new net.Server()

test('Start Gopher server', (t) => {
  gopherServer.listen(0, () => {
    const { address, port, family } = gopherServer.address()
    const host = family === 'IPv6'
      ? `[${address}]:${port}`
      : `${address}:${port}`
    console.log(`[SERVER] Listening at ${host}`)
    t.end()
  })
  gopherServer.on('connection', (socket) => {
    socket.on('data', (chunk) => {
      console.log(`[SERVER] RECV: ${chunk}`)
      t.notOk(/\r\n/.test(chunk.toString()))
      socket.end('3Invalid Gopher\t\t\t\r\n.\r\n')
    })
  })
})

test('GoT client connecting to non-GoT Gopher server', (t) => {
  const client = tls.connect({
    host: 'localhost',
    port: gopherServer.address().port,
    servername: 'example.com',
    ALPNProtocols: ['gopher'],
    rejectUnauthorized: false
  })
  client.on('error', (error) => t.ok(error))
  client.on('close', (hadError) => {
    t.is(hadError, true)
    t.end()
  })
})

test('Stop Gopher server', (t) => {
  gopherServer.close(t.end)
})
